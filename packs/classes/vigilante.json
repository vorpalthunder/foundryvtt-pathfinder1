{
  "_id": "Yf5Wv9X09SUHYCqM",
  "name": "Vigilante",
  "type": "class",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p>Being a renowned hero can be dangerous to your health and prosperity. Fighting against corruption and the evil that lurks within society makes you a target and, even worse, puts your friends and family in danger. For those who must maintain a social persona, being a part of the greater community while secretly fighting against powerful forces within it requires leading a double life.</p>\n<p>By day, the vigilante maneuvers through society, dealing with other nobles or influential individuals. By night, he dons a disguise and an utterly different set of goals, taking the fight to his foes and solving problems with a blade when words will not suffice.</p>\n<p>Game Masters should consider carefully whether or not a vigilante will make for a good fit with their campaign. The class is one that requires a degree of social aptitude and roleplaying to make full use of its potential.</p>\n<p>Campaigns that focus more on wilderness exploration, travel, or dungeon delving and that are lighter on politics, negotiation, and manipulation might require a vigilante player to put in additional effort to make full use of his class features. Alternatively, a vigilante is uniquely suited to make for a powerful villain, hidden by day behind a mask of civility and a terror at night, free to commit terrible acts without risking discovery.</p>\n<p>For players, the vigilante offers a unique opportunity to take on the role of a character with a hidden side, and whose life is committed to a secret agenda that he must struggle to advance in a complex world. Not every problem can be solved with a dagger in the dark, and even the most stubborn foe might be become an ally with the proper bribe. For the vigilante, these tasks are both within reach as long as you learn to properly use your dual nature and hidden skills to your fullest advantage.</p>\n<p>Life can be unfair. Think of the starving peasants forced to toil for the local baron or the common laborers tasked with building the king’s newest palace for a mere handful of copper pieces each week. There are those who see these injustices and do nothing. There are those who are willing to reap the rewards obtained through the suffering of others.</p>\n<p>Then there are those who see inequality and find themselves driven to take action, outside the law if necessary. These vigilantes operate in plain sight, hiding behind respectable personas by day, but donning alternate guises by night to right the wrongs they see all around them.</p>\n<p>Not all vigilantes are out to make the world a better place. Some criminals hide behind the pretense of being ordinary folk, only to become terrors in the shadows, stealing and killing to fulfill some dark agenda. In either case, the vigilante is a character of two natures—the face that everyone knows and the mask that inspires fear.</p>\n<p><strong>Role</strong>: A vigilante can take on many tasks within a group. Most are skilled at negotiating delicate social situations and courtly intrigue, but they can also serve as stealthy spies or even brutish warriors in dangerous environments.</p>\n<p><strong>Alignment</strong>: Any.</p>\n<p><strong>Hit Die</strong>: d8.</p>\n<p><strong>Starting Wealth</strong>: 5d6 × 10 (average 175 gp).</p>\n<h2><span id=\"Class_Skills\">Class Skills</span></h2>\n<p>A vigilante’s class skills are <em>Acrobatics</em>&nbsp;(<em>Dex</em>), <em>Appraise</em>&nbsp;(<em>Int</em>), <em>Bluff</em>&nbsp;(<em>Cha</em>), <em>Climb</em>&nbsp;(<em>Str</em>), <em>Craft</em>&nbsp;(<em>Int</em>), <em>Diplomacy</em>&nbsp;(<em>Cha</em>), <em>Disable Device</em>&nbsp;(<em>Dex</em>), <em>Disguise</em>&nbsp;(<em>Cha</em>), <em>Escape Artist</em>&nbsp;(<em>Dex</em>), <em>Intimidate</em>&nbsp;(<em>Cha</em>), <em>Knowledge</em>&nbsp;(dungeoneering) (<em>Int</em>), <em>Knowledge</em>&nbsp;(engineering) (<em>Int</em>), <em>Knowledge</em>&nbsp;(local) (<em>Int</em>), <em>Knowledge</em>&nbsp;(nobility) (<em>Int</em>), <em>Perception</em>&nbsp;(<em>Wis</em>), <em>Perform</em>&nbsp;(<em>Cha</em>), <em>Profession</em>&nbsp;(<em>Wis</em>), <em>Ride</em>&nbsp;(<em>Dex</em>), <em>Sense Motive</em>&nbsp;(<em>Wis</em>), <em>Sleight of Hand</em>&nbsp;(<em>Dex</em>), <em>Stealth</em>&nbsp;(<em>Dex</em>), <em>Survival</em>&nbsp;(<em>Wis</em>), <em>Swim</em>&nbsp;(<em>Str</em>), and <em>Use Magic Device</em>&nbsp;(<em>Cha</em>).</p>\n<p><strong>Skill Ranks per Level</strong>: 6 +&nbsp;<em>Int</em>&nbsp;modifier.</p>"
    },
    "tags": [],
    "changes": [],
    "links": {
      "children": [],
      "classAssociations": [
        {
          "id": "pf1.class-abilities.caHXpWVlJLxRKvOS",
          "dataType": "compendium",
          "name": "Dual Identity",
          "img": "systems/pf1/icons/items/inventory/mask-bone.jpg",
          "level": 1,
          "_index": 0
        },
        {
          "id": "pf1.class-abilities.TMpfSlpC7To6uXGE",
          "dataType": "compendium",
          "name": "Seamless Guise",
          "img": "systems/pf1/icons/skills/shadow_11.jpg",
          "level": 1,
          "_index": 1
        },
        {
          "id": "pf1.class-abilities.2uDIHsepFL6PRyRj",
          "dataType": "compendium",
          "name": "Social Talent",
          "img": "systems/pf1/icons/skills/light_07.jpg",
          "level": 1,
          "_index": 2
        },
        {
          "id": "pf1.class-abilities.vTE94sQboMmdAakT",
          "dataType": "compendium",
          "name": "Vigilante Specialization",
          "img": "systems/pf1/icons/skills/blue_35.jpg",
          "level": 1,
          "_index": 3
        },
        {
          "id": "pf1.class-abilities.o4FeJzhEdocIO4mY",
          "dataType": "compendium",
          "name": "Vigilante Talent",
          "img": "systems/pf1/icons/skills/shadow_17.jpg",
          "level": 2,
          "_index": 4
        },
        {
          "id": "pf1.class-abilities.4U69gg3altKZvFHD",
          "dataType": "compendium",
          "name": "Unshakable",
          "img": "systems/pf1/icons/skills/blue_27.jpg",
          "level": 3,
          "_index": 5
        },
        {
          "id": "pf1.class-abilities.qCl2cw0leReIXuRn",
          "dataType": "compendium",
          "name": "Startling Appearance",
          "img": "systems/pf1/icons/skills/blood_13.jpg",
          "level": 5,
          "_index": 6
        },
        {
          "id": "pf1.class-abilities.2tq8HQ4ZQpiL3njr",
          "dataType": "compendium",
          "name": "Frightening Appearance",
          "img": "systems/pf1/icons/skills/red_02.jpg",
          "level": 11,
          "_index": 7
        },
        {
          "id": "pf1.class-abilities.fCaw2ldaQlkktvsB",
          "dataType": "compendium",
          "name": "Stunning Appearance",
          "img": "systems/pf1/icons/skills/affliction_18.jpg",
          "level": 17,
          "_index": 8
        },
        {
          "id": "pf1.class-abilities.xrruZUG1zqIHe35h",
          "dataType": "compendium",
          "name": "Vengeance Strike",
          "img": "systems/pf1/icons/skills/fire_13.jpg",
          "level": 20,
          "_index": 9
        }
      ]
    },
    "tag": "vigilante",
    "useCustomTag": true,
    "armorProf": {
      "value": ["lgt", "med", "shl"]
    },
    "weaponProf": {
      "value": ["sim", "mar"]
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "bab": "med",
    "skillsPerLevel": 6,
    "savingThrows": {
      "ref": {
        "value": "high"
      },
      "will": {
        "value": "high"
      }
    },
    "classSkills": {
      "acr": true,
      "apr": true,
      "art": true,
      "blf": true,
      "clm": true,
      "crf": true,
      "dip": true,
      "dev": true,
      "dis": true,
      "esc": true,
      "fly": false,
      "han": false,
      "hea": false,
      "int": true,
      "kar": false,
      "kdu": true,
      "ken": true,
      "kge": false,
      "khi": false,
      "klo": true,
      "kna": false,
      "kno": true,
      "kpl": false,
      "kre": false,
      "lin": false,
      "lor": true,
      "per": true,
      "prf": true,
      "pro": true,
      "rid": true,
      "sen": true,
      "slt": true,
      "spl": false,
      "ste": true,
      "sur": true,
      "swm": true,
      "umd": true
    }
  }
}
