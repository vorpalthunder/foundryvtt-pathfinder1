import { ActorBasePF } from "./actor-base.mjs";

/**
 * Basic actor with no built-in functionality.
 */
export class BasicActorPF extends ActorBasePF {}
